certifi<=2018.4.16
elasticsearch>=7.0.0,<8.0.0
elasticsearch-dsl>=7.0.0,<8.0.0
requests<=2.25
