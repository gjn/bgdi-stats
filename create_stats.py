from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

from datetime import datetime, timedelta
from shutil import copyfile
import subprocess
import time
import os

es_apikey = os.environ['ES_APIKEY']


bots = "NOT agent: \"PerformanceMonitorBot\" AND NOT agent: \"http://www.pingdom.com/\" AND NOT agent: \"ELB-HealthChecker\" AND NOT agent: \"bingbot\" AND NOT agent: \"http://www.uptimerobot.com/\" AND NOT agent: \"i4ds-bot\" AND NOT agent: \"Googlebot\" AND NOT agent: \"http://www.semrush.com/bot.html\" AND NOT agent: \"YandexBot\" AND NOT agent: \"AhrefsBot\""
bots_new = "NOT user_agent: \"PerformanceMonitorBot\" AND NOT user_agent: \"http://www.pingdom.com/\" AND NOT user_agent: \"ELB-HealthChecker\" AND NOT user_agent: \"bingbot\" AND NOT user_agent: \"http://www.uptimerobot.com/\" AND NOT user_agent: \"i4ds-bot\" AND NOT user_agent: \"Googlebot\" AND NOT user_agent: \"http://www.semrush.com/bot.html\" AND NOT user_agent: \"YandexBot\" AND NOT user_agent: \"AhrefsBot\""

defs = [{
        'id': 'uniqueIPs',
        'type': 'cardinality',
        'label': 'Unique Client IPs',
        'index': 'varnish-*',
        'field': 'clientIP',
        'bots': bots,
        'datefield': 'timegenerated'
    },{
        'id': 'uniqueHosts',
        'type': 'cardinality',
        'label': 'Unique Hosts',
        'index': 'varnish-*',
        'field': 'refererHost',
        'bots': bots,
        'datefield': 'timegenerated'

    },{
        'id': 'uniqueDomains',
        'type': 'cardinality',
        'label': 'Unique Domains',
        'index': 'varnish-*',
        'field': 'refererDomain',
        'bots': bots,
        'datefield': 'timegenerated'

    },{
        'id': 'allApi3',
        'type': 'count',
        'label': 'All Successful API3 requests',
        'index': 'varnish-*',
        'queries': [{"query_string":{"query": "(requestHostHeader: \"api3.geo.admin.ch\" OR requestHostHeader: \"mf-chsdi3.prod.bgdi.ch\")"}},
                    {"match_phrase":{"response":{"query":200}}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'

    },{
        'id': 'allSearch',
        'type': 'count',
        'label': 'All Successful Search requests',
        'index': 'varnish-*',
        'queries': [{"query_string":{"query": "(requestHostHeader: \"api3.geo.admin.ch\" OR requestHostHeader: \"mf-chsdi3.prod.bgdi.ch\")"}},
                    {"match_phrase":{"requestPath": {"query": "SearchServer"}}},
                    {"match_phrase":{"response":{"query":200}}},
                    {"match_phrase":{"verb":{"query":"GET"}}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'

    },{
        'id': 'allAlti',
        'type': 'count',
        'label': 'All Successful Alti requests',
        'index': 'varnish-*',
        'queries': [{"query_string":{"query": "(requestHostHeader: \"api3.geo.admin.ch\" OR requestHostHeader: \"mf-chsdi3.prod.bgdi.ch\")"}},
                    {"query_string":{"query": "requestPath: \"rest/services/height\" OR requestPath: \"rest/services/profile.json\" OR requestPath: \"rest/services/profile.csv\""}},
                    {"match_phrase":{"response":{"query":200}}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'


    },{
        'id': 'allReverseGeocoding',
        'type': 'count',
        'label': 'All Successful Reverse Geocoding requests',
        'index': 'varnish-*',
        'queries': [{"query_string":{"query": "(requestHostHeader: \"api3.geo.admin.ch\" OR requestHostHeader: \"mf-chsdi3.prod.bgdi.ch\")"}},
                    {"query_string":{"query": "requestPath: \"identify\""}},
                    {"match_phrase":{"response":{"query":200}}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'

    },{
        'id': 'pdfCreated',
        'type': 'count',
        'label': 'PDFs Created',
        'index': 'bgdi-access-print*',
        'queries': [{"match_phrase":{"x_host_header": {"query": "print.geo.admin.ch"}}},
                     {"match_phrase":{"cs_method":{"query":"POST"}}},
                     {"match_phrase":{"cs_uri_stem.keyword":{"query":"/print/create.json"}}},
                     {"match_phrase":{"sc_status":{"query":200}}},
                     {"query_string":{"query": bots_new}}],
        'datefield': '@timestamp'
    },{
        'id': 'kmlCreated',
        'type': 'count',
        'label': 'KMLs Created',
        'index': 'bgdi-access-*',
        'queries': [{"match_phrase":{"x_host_header": {"query": "public.geo.admin.ch"}}},
                    {"match_phrase":{"cs_method":{"query":"POST"}}},
                    {"match_phrase":{"cs_uri_stem.keyword":{"query":"/api/kml/admin"}}},
                     {"match_phrase":{"sc_status":{"query": 201}}},
                     {"query_string":{"query": bots_new}}],
        'datefield': '@timestamp'
    },{
        'id': 'sharedMap',
        'type': 'count',
        'label': 'Maps shared via shortener',
        'index': 'bgdi-access-*',
        'queries': [{"match_phrase":{"x_host_header": {"query": "s.geo.admin.ch"}}},
                    {"match_phrase":{"cs_method":{"query":"POST"}}},
                    {"match_phrase":{"cs_uri_stem.keyword":{"query":"/"}}},
                     {"match_phrase":{"sc_status":{"query": 201}}},
                     {"query_string":{"query": bots_new}}],
        'datefield': '@timestamp'
    },{
        'id': '3xx',
        'type': 'count',
        'label': 'All 3xx redirects',
        'index': 'varnish-*',
        'queries': [{"query_string":{"query": "requestHostHeader: \"api3.geo.admin.ch\""}},
                    {"query_string":{"query": ("response: [300 TO 399]")}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated',
    },{
        'id': '4xx',
        'type': 'count',
        'label': 'All 4xx redirects',
        'index': 'varnish-*',
        'queries': [{"query_string":{"query": "requestHostHeader: \"api3.geo.admin.ch\""}},
                    {"query_string":{"query": ("response: [400 TO 499]")}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated',
    },{
        'id': '5xx',
        'type': 'count',
        'label': 'All 5xx redirects',
        'index': 'varnish-*',
        'queries': [{"query_string":{"query": "requestHostHeader: \"api3.geo.admin.ch\""}},
                    {"query_string":{"query": ("response: [500 TO 599]")}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'
        
    },{
        'id': 'wmtsVIP',
        'type': 'count',
        'label': 'All VIP wmts access logs from Cloudfront',
        'index': 'wmts-*',
        'queries': [{"query_string":{"query": "requestHostHeader: \"cloudfront.prod.bgdi.ch\" AND  NOT requestHost: \"mwks6dv2y5dsbbgg-vectortiles.s3-eu-west-1.amazonaws.com\" AND NOT requestHost: \"ioazqwe8i7q2zof-tms3d.s3-eu-west-1.amazonaws.com\""}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'
     },{
        'id': 'wmtsNONVIP',
        'type': 'count',
        'label': 'All NON VIP wmts access logs',
        'index': 'swisstopo_wmts_access_logs-*',
        'queries': [{"query_string":{"query": "requestHostHeader:/wmts.*/ AND (requestHostHeader:/wmts[1-9]\.*/ OR requestHostHeader: \"wmts.geo.admin.ch\")"}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'
    },{
        'id': 'wmtsFromToD',
        'type': 'count',
        'label': 'All ToD requests',
        'index': 'wmts-*',
        'queries': [{"query_string":{"query": "responseServer: *nginx* AND ((requestHostHeader: \"cloudfront.prod.bgdi.ch\" AND  NOT requestHost: \"mwks6dv2y5dsbbgg-vectortiles.s3-eu-west-1.amazonaws.com\" AND NOT requestHost: \"ioazqwe8i7q2zof-tms3d.s3-eu-west-1.amazonaws.com\") OR (requestHostHeader:/wmts.*/ AND (requestHostHeader:/wmts[1-9]\.*/ OR requestHostHeader: \"wmts.geo.admin.ch\")))"}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'
    },{
        'id': 'wmtsAll3xx',
        'type': 'count',
        'label': 'All WMTS 3xx requests',
        'index': 'wmts-*',
        'queries': [{"query_string":{"query": "response: [300 TO 399] AND ((requestHostHeader: \"cloudfront.prod.bgdi.ch\" AND  NOT requestHost: \"mwks6dv2y5dsbbgg-vectortiles.s3-eu-west-1.amazonaws.com\" AND NOT requestHost: \"ioazqwe8i7q2zof-tms3d.s3-eu-west-1.amazonaws.com\") OR (requestHostHeader:/wmts.*/ AND (requestHostHeader:/wmts[1-9]\.*/ OR requestHostHeader: \"wmts.geo.admin.ch\")))"}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'
    },{
        'id': 'wmtsAll4xx',
        'type': 'count',
        'label': 'All WMTS 4xx requests',
        'index': 'wmts-*',
        'queries': [{"query_string":{"query": "response: [400 TO 499] AND ((requestHostHeader: \"cloudfront.prod.bgdi.ch\" AND  NOT requestHost: \"mwks6dv2y5dsbbgg-vectortiles.s3-eu-west-1.amazonaws.com\" AND NOT requestHost: \"ioazqwe8i7q2zof-tms3d.s3-eu-west-1.amazonaws.com\") OR (requestHostHeader:/wmts.*/ AND (requestHostHeader:/wmts[1-9]\.*/ OR requestHostHeader: \"wmts.geo.admin.ch\")))"}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'
    },{
        'id': 'wmtsAll5xx',
        'type': 'count',
        'label': 'All WMTS 5xx requests',
        'index': 'wmts-*',
        'queries': [{"query_string":{"query": "response: [500 TO 599] AND ((requestHostHeader: \"cloudfront.prod.bgdi.ch\" AND  NOT requestHost: \"mwks6dv2y5dsbbgg-vectortiles.s3-eu-west-1.amazonaws.com\" AND NOT requestHost: \"ioazqwe8i7q2zof-tms3d.s3-eu-west-1.amazonaws.com\") OR (requestHostHeader:/wmts.*/ AND (requestHostHeader:/wmts[1-9]\.*/ OR requestHostHeader: \"wmts.geo.admin.ch\")))"}},
                    {"query_string":{"query": bots}}],
        'datefield': 'timegenerated'
    },{
        'id': 'wmtsToD5xx',
        'type': 'count',
        'label': 'All ToD 5xx requests',
        'index': 'wmts-*',
        'queries': [{"query_string":{"query": "responseServer: *nginx* AND response: [500 TO 599] AND ((requestHostHeader: \"cloudfront.prod.bgdi.ch\" AND  NOT requestHost: \"mwks6dv2y5dsbbgg-vectortiles.s3-eu-west-1.amazonaws.com\" AND NOT requestHost: \"ioazqwe8i7q2zof-tms3d.s3-eu-west-1.amazonaws.com\") OR (requestHostHeader:/wmts.*/ AND (requestHostHeader:/wmts[1-9]\.*/ OR requestHostHeader: \"wmts.geo.admin.ch\")))"}},
                    {"query_string":{"query": bots}}
                    ],
        'datefield': 'timegenerated'
    },{
        'id': 'uniqueIPsBGDI',
        'type': 'cardinality',
        'label': 'Unique Client IPs for BGDI indices',
        'index': 'bgdi-access-*',
        'field': 'c_ip',
        'bots': bots_new,
        'datefield': '@timestamp'
    }
]

def csvheader():
    ret = 'date,'
    for defi in defs:
        ret = ret + defi['id'] + ','
    return ret + '\n'


def getStatline(dt, datestr):
    range_from = dt.strftime('%Y-%m-%dT00:00:00.000Europe/Zurich')
    range_to = dt.strftime('%Y-%m-%dT23:59:59.999Europe/Zurich')
    line = datestr + ','
    for d in defs:
        date_range = {d['datefield']:{"gte":range_from,"lte":range_to}}
        client = Elasticsearch('https://elasticsearch.apps.c2c-management.swisstopo.cloud:443/' + d['index'] + '/',headers={'Authorization':'ApiKey ' + es_apikey},use_ssl=True, verify_certs=False)
        if 'cardinality' in d['type']:
            response = client.search(
                    request_timeout=30,
                    ignore_unavailable=True,
                    body= {
                        "query": {
                            "bool": {
                                "must": [{"match_all":{}},
                                     {"query_string": {"query": d['bots']}},
                                     {"range": date_range}]}},
                        "aggs": {
                            "client_count": {
                                "cardinality": {
                                    "field":d['field']
                                }
                            }
                        }
                }
            )
            print(d['label'] + ': ' + str(response['aggregations']['client_count']['value']))
            line = line + str(response['aggregations']['client_count']['value']) + ','
        else:
            queries = d['queries']
            found = False
            for query in queries:
                if 'range' in query:
                    query['range'] = date_range
                    found = True
            if not found:
                queries.append({"range": date_range})
            response = client.count(
                    request_timeout=30,
                    ignore_unavailable=True,
                    body= {
                        "query": {
                            "bool": {"must": queries}
                    }
                }
            )
            print(d['label'] + ': ' + str(response['count']))
            line = line + str(response['count']) + ','
    line = line + '\n'
    print(line)
    return line

dateformat = '%Y%m%d'

def getStats(lines):
    now = datetime.now()
    nowstr = now.strftime(dateformat)
    lastline = lines[-1]
    lastdate = datetime.strptime(lastline.split(',')[0],dateformat)
    lines = lines[:-1]
    current = lastdate
    while current >= lastdate and current < now:
        currstr = current.strftime(dateformat)
        line = getStatline(current, currstr)
        lines.append(line)
        current = current + timedelta(days=1)
    return lines

# Read CSV file
fn_raw = 'out/current_raw.csv'
fn_gzip = 'out/current.csv'

def upload():
    # create gzip version
    now = datetime.now()
    try:
        sub = subprocess.check_output('gzip -c ' + fn_raw + ' > ' + fn_gzip, shell = True)
        print(sub)
    except Exception as e:
        print('exception while creating zip version of file', e)

    if os.path.isfile(fn_gzip):
        copyfile(fn_gzip, 'out/current_' + now.strftime(dateformat) + '.csv')

    print('Uploading...')

    try:
        sub = subprocess.check_output('aws s3 sync --cache-control "public, max-age=120" --content-encoding "gzip" --profile ltjeg_aws_admin out s3://cms.geo.admin.ch/stats/counts/', shell = True)
        print(sub)

    except Exception as e:
        print('exception while trying to upload...', e)


def main():
    lines = []
    if os.path.isfile(fn_raw):
        with open(fn_raw, 'r') as readf:
            lines = readf.readlines()
    
    if len(lines) == 0:
        lines.append(csvheader())

    while True:
        try:
            lines = getStats(lines)
            with open(fn_raw, 'w+') as writef:
                writef.writelines(lines)
            upload()
        except Exception as e:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            print message
        # waith 200 seconds 
        time.sleep(200.0)

if __name__ == '__main__':
    main() 

