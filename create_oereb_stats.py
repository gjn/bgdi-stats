from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

from datetime import datetime, timedelta
from shutil import copyfile
import subprocess
import time
import os
import requests

es_apikey = os.environ['ES_APIKEY']

client = Elasticsearch('https://elasticsearch.apps.c2c-management.swisstopo.cloud:443/varnish-*/',headers={'Authorization':'ApiKey ' + es_apikey},use_ssl=True, verify_certs=False)

bots = "NOT agent: \"PerformanceMonitorBot\" AND NOT agent: \"http://www.pingdom.com/\" AND NOT agent: \"ELB-HealthChecker\" AND NOT agent: \"bingbot\" AND NOT agent: \"http://www.uptimerobot.com/\" AND NOT agent: \"i4ds-bot\" AND NOT agent: \"Googlebot\" AND NOT agent: \"http://www.semrush.com/bot.html\" AND NOT agent: \"YandexBot\" AND NOT agent: \"AhrefsBot\""

def csvheader():
    ret = 'date,layer,geometry,count'
    return ret + '\n'

def executeQuery(date_range):
    body= {
        "query": {
            "bool": {
                "must": [{"match_all":{}},
                         {"query_string": {"query": bots}},
                         {"range": date_range},
                         {"query_string": {"query": "response: 200"}},
                         {"query_string": {"query": "requestHost: api"}},
                         {"query_string": {"query": "NOT requestPath: qrcodegenerator"}},
                         {"query_string": {"query": "NOT requestPath: shorten.json"}}
                    ]}}
    }
    body['query']['bool']['must'].append({"query_string": {"query": "requestArgumentString: interlis"}})
    body['query']['bool']['must'].append({"query_string": {"query": "requestPath: identify"}})
    print('doing oereb stats query')
    return client.search(
                request_timeout = 30,
                ignore_unavailable = True,
                size=10000,
                body= body
        )
 


def getStatlines(dt, datestr):
    range_from = dt.strftime('%Y-%m-%dT00:00:00.000Europe/Zurich')
    range_to = dt.strftime('%Y-%m-%dT23:59:59.999Europe/Zurich')
    date_range = {"timegenerated":{"gte":range_from,"lte":range_to}}
    lines = []
    currentNum = 0

    query_res = executeQuery(date_range)
    #print(query_res)
    print(type(query_res['hits']['hits']))
    print(len(query_res['hits']['hits']))
    #print(query_res['hits']['hits'][0])
    #print("hits: " + query_res['hits'])
    #for value, key in query_res['hits']['hits'].items():
    #    print(type(value))
    daily_dict = {
    }
    count = 0
    for hit in query_res['hits']['hits']:
        count += 1
        print(count)
        if '_source' not in hit:
            print(hit)
            print('_source not in hit')
            continue
        if 'requestArguments' not in hit['_source']:
            print(hit)
            print('requestArguemnets not in source')
            continue
        if 'layers' not in hit['_source']['requestArguments']:
            print(hit)
            print('layers not in requestArgunments')
            continue
        if 'geometry' not in hit['_source']['requestArguments']:
            print(hit)
            print('geometry not in requestArguments')
        if len(hit['_source']['requestArguments']['layers']) <= 0:
            print('no layers in results')
            continue

        layers = hit['_source']['requestArguments']['layers'][0]
        print(layers)
        if layers not in daily_dict:
            daily_dict[layers] = { }
        geometry = hit['_source']['requestArguments']['geometry'][0]
        if geometry not in daily_dict[layers]:
            daily_dict[layers][geometry] = 0
        daily_dict[layers][geometry] += 1

    print('being here...finishing for ' + datestr)
    for layername, laydic in daily_dict.items():
        for geometry, count in laydic.items():
            lines.append(datestr + ',' + layername + ',' + geometry.replace(',','-') + ',' + str(count) + '\n')
    print(lines)
    return lines

dateformat = '%Y%m%d'

def getStats(lines):
    now = datetime.now()
    lastline = lines[-1]
    lastdate = datetime.strptime(lastline.split(',')[0],dateformat)
    current = lastdate + timedelta(days=1)
    print(str(now) + ' | lastline: ' + lastline)
    while (current + timedelta(days=1)) < (now - timedelta(hours=3)):
        currstr = current.strftime(dateformat)
        newlines = getStatlines(current, currstr)
        lines.extend(newlines)
        with open(fn_raw, 'w+') as writef:
            writef.writelines(lines)
            upload()
 
        current = current + timedelta(days=1)
    return lines

# Read CSV file
fn_raw = 'out/oereb_current_raw.csv'
fn_gzip = 'out/oereb_current.csv'

def upload():
    # create gzip version
    now = datetime.now()
    try:
        sub = subprocess.check_output('gzip -c ' + fn_raw + ' > ' + fn_gzip, shell = True)
        print(sub)
    except Exception as e:
        print('exception while creating zip version of file', e)

    if os.path.isfile(fn_gzip):
        copyfile(fn_gzip, 'out/oereb_current_' + now.strftime(dateformat) + '.csv')

    print('Uploading...')

    try:
        sub = subprocess.check_output('aws s3 sync --cache-control "public, max-age=120" --content-encoding "gzip" --profile ltjeg_aws_admin out s3://cms.geo.admin.ch/stats/counts/', shell = True)
        print(sub)

    except Exception as e:
        print('exception while trying to upload...', e)


def main():
    lines = []
    if os.path.isfile(fn_raw):
        with open(fn_raw, 'r') as readf:
            lines = readf.readlines()
    
    if len(lines) == 0:
        lines.append(csvheader())

    while True:
        try:
            lines = getStats(lines)
        except Exception as e:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            print message
        # waith 200 seconds 
        time.sleep(3600)

if __name__ == '__main__':
    main() 

