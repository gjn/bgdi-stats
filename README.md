This scripts create some stats aggregation for the BGDI using the data stored in
Elastic Search.

# Install
Create a python 2.7 environment

'''
virtualenv -p /usr/bin/python .venv
source .venv/bin/activate
pip install -r requirements.txt
'''

# authentification
You need an API Token Key to access the elastic Search instance. Note: currently,
the SSL certificate is not valid anymore on our ES instance. That's why there
are warnings during execution. Once the certificate is working again, ssl
validation can be activated again.

# create_stats.py
Creates aggreation stats for some service. Output is written to cms.geo.admin.ch

In your .venv, execute 'python create_stats.py'

# create_perlayer_stats.py
Creates service layer usage per layer. The layers are dynamically loaded from the
currently active configuration in production. Output is written to cms.geo.admin.ch
Each layer has an entry per day and different usages (wms, api, identify, etc.)

In your .venv, execute 'python create_perlayer_stats.py'


# create_oere_.stats.py
Creates oereb layer specific stats. Output is written to cms.geo.admin.ch

In your .venv, execute 'python create_oereb_stats.py'


