from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

from datetime import datetime, timedelta
from shutil import copyfile
import subprocess
import time
import os
import requests

es_apikey = os.environ['ES_APIKEY']

client = Elasticsearch('https://elasticsearch.apps.c2c-management.swisstopo.cloud:443/varnish-*/',headers={'Authorization':'ApiKey ' + es_apikey},use_ssl=True, verify_certs=False)

oereb_layers = [
    'all:ch.astra.baulinien-nationalstrassen.oereb',
    'all:ch.astra.projektierungszonen-nationalstrassen.oereb',
    'all:ch.bav.baulinien-eisenbahnanlagen.oereb',
    'all:ch.bav.kataster-belasteter-standorte-oev.oereb',
    'all:ch.bav.projektierungszonen-eisenbahnanlagen.oereb',
    'all:ch.bazl.baulinien-flughafenanlagen.oereb',
    'all:ch.bazl.kataster-belasteter-standorte-zivilflugplaetze.oereb',
    'all:ch.bazl.projektierungszonen-flughafenanlagen.oereb',
    'all:ch.bazl.sicherheitszonenplan.oereb',
    'all:ch.vbs.kataster-belasteter-standorte-militaer.oereb'
]

bots = "NOT agent: \"PerformanceMonitorBot\" AND NOT agent: \"http://www.pingdom.com/\" AND NOT agent: \"ELB-HealthChecker\" AND NOT agent: \"bingbot\" AND NOT agent: \"http://www.uptimerobot.com/\" AND NOT agent: \"i4ds-bot\" AND NOT agent: \"Googlebot\" AND NOT agent: \"http://www.semrush.com/bot.html\" AND NOT agent: \"YandexBot\" AND NOT agent: \"AhrefsBot\""

defs = {
    "wms_count": { "host": "wms", "pathfilter": None },
    "api_count": { "host": "api", "pathfilter": None },
    "legend_count": { "host": "api", "pathfilter": "requestPath: legend" },
    "identify_count": { "host": "api", "pathfilter": "requestPath: identify" },
    "find_count": { "host": "api", "pathfilter": "requestPath: find" },
    "tooltip_count": { "host": "api", "pathfilter": "requestPath: htmlpopup" },
    "extended_tooltip_count": { "host": "api", "pathfilter": "requestPath: extendedhtmlpopup" },
    "featuresearch_count": { "host": "api", "pathfilter": "requestPath: searchserver" },
    "cacheupdate_count": { "host": "api", "pathfilter": "requestPath: cacheupdate" },
    "feature_count": { "host": "api", "pathfilter": "NOT requestPath: legend AND NOT requestPath: identify AND NOT requestPath: find AND NOT requestPath: htmlpopup AND NOT requestPath: extendedhtmlpopup AND NOT requestPath: searchserver AND NOT requestPath: cacheupdate" }
}

def csvheader():
    ret = 'date,layer,wmscount,apicount'
    return ret + '\n'

def chunk_based_on_size(lst, n):
    for x in range(0, len(lst), n):
        each_chunk = lst[x: n+x]
        if len(each_chunk) < n:
            each_chunk = each_chunk + [None for y in range(n-len(each_chunk))]
        yield each_chunk

def executeQuery(host, date_range, aggs, pathfilter):
    body= {
        "query": {
            "bool": {
                "must": [{"match_all":{}},
                         {"query_string": {"query": bots}},
                         {"range": date_range},
                         {"query_string": {"query": "response: 200"}},
                         {"query_string": {"query": "requestHost: " + host}},
                         {"query_string": {"query": "NOT requestPath: qrcodegenerator"}},
                         {"query_string": {"query": "NOT requestPath: shorten.json"}}
                    ]}},
        "aggs": aggs
    }
    if pathfilter is not None:
        body['query']['bool']['must'].append({"query_string": {"query": pathfilter}})
    return client.search(
                request_timeout = 30,
                ignore_unavailable = True,
                body= body
        )
 


def getStatlines(dt, datestr, layers):
    range_from = dt.strftime('%Y-%m-%dT00:00:00.000Europe/Zurich')
    range_to = dt.strftime('%Y-%m-%dT23:59:59.999Europe/Zurich')
    date_range = {"timegenerated":{"gte":range_from,"lte":range_to}}
    lines = []
    numLayers = len(layers)
    currentNum = 0

    chunks = chunk_based_on_size(layers, 50)
    for chunk in chunks:
         
        aggs = {
            "2": {
                "filters": {
                    "filters": {
                    }
                }
            }
        }
        deffilters = aggs["2"]["filters"]["filters"]

        aggs = {
            "interactions": {
                "adjacency_matrix": {
                    "filters": {
                    }
                }
            }
        }
        deffilters = aggs["interactions"]["adjacency_matrix"]["filters"]
            

        for layer in chunk:
            if layer is None:
                continue
            currentNum += 1
            ff = "requestArgumentString: \""
            ff += layer
            ff += "\" OR requestPath: \""
            ff += layer
            ff += "\""
            deffilters[layer] = {
                 "query_string": {
                     "query": ff
                }
            }
    
        results = {}
        print("Get stats for a chunk of 50 layers, " + str(currentNum) + " of " + str(numLayers) + " for date " + datestr)
        for key, dd in defs.items():
            results[key] = executeQuery(dd['host'], date_range, aggs, dd['pathfilter'])
        
        resall = {}
        for key, result in results.items():
            for bucket in result["aggregations"]["interactions"]["buckets"]:
                if '&' not in bucket["key"]:
                    if bucket["key"] not in resall:
                        resall[bucket["key"]] = {
                                "wms_count": 0,
                                "api_count": 0,
                                "legend_count": 0,
                                "identify_count": 0,
                                "find_count": 0,
                                "tooltip_count": 0,
                                "extended_tooltip_count": 0,
                                "featuresearch_count": 0,
                                "cacheupdate_count": 0,
                                "feature_count":0
                            }
                    resall[bucket["key"]][key] = bucket["doc_count"]


        for layername, result in resall.items():
            lines.append(datestr + ',' + layername
                        + ',' + str(result["wms_count"])
                        + ',' + str(result["api_count"])
                        + ',' + str(result["legend_count"])
                        + ',' + str(result["identify_count"])
                        + ',' + str(result["find_count"])
                        + ',' + str(result["tooltip_count"])
                        + ',' + str(result["extended_tooltip_count"])
                        + ',' + str(result["featuresearch_count"])
                        + ',' + str(result["cacheupdate_count"])
                        + ',' + str(result["feature_count"])+ "\n")
            print(lines[-1])
    return lines

dateformat = '%Y%m%d'

# Read CSV file
fn_raw = 'out/layers_current_raw.csv'
fn_gzip = 'out/layers_current.csv'
fn_monthlyraw = 'out/layers_monthly_raw.csv'
fn_monthlygzip = 'out/layers_monthly.csv'
fn_yearlyraw = 'out/layers_yearly_raw.csv'
fn_yearlygzip = 'out/layers_yearly.csv'


# Aggregate daily stats for months and years
def aggregate(lines):
    monthly = {}
    yearly = {}
    headers = lines[0].split(',')[2:]
    for line in lines[1:]:
        lineparts = line.split(',')
        if len(lineparts) is not 12:
            continue
        if lineparts[0].isdigit() is not True:
            continue
        monthstr = lineparts[0][0:6]
        yearstr = lineparts[0][0:4]
        if monthstr not in monthly:
            monthly[monthstr] = {}
        if yearstr not in yearly:
            yearly[yearstr] = {}
        
        monstat = monthly[monthstr]
        yeastat = yearly[yearstr]
        if lineparts[1] not in monstat:
            monstat[lineparts[1]] = {}
            for header in headers:
                monstat[lineparts[1]][header] = 0
        if lineparts[1] not in yeastat:
            yeastat[lineparts[1]] = {}
            for header in headers:
                yeastat[lineparts[1]][header] = 0
        for index, value in enumerate(lineparts[2:]):
            monstat[lineparts[1]][headers[index]] += int(value)
            yeastat[lineparts[1]][headers[index]] += int(value)

    outlines = []
    for monkey, monstat in monthly.items():
        outlines.append(lines[0]) #header
        for layername, stats in monstat.items():
            statstr = ""
            for statkey in headers:
                statstr += (',' + str(stats[statkey]))
            outlines.append(monkey + ',' + layername + statstr + '\n')

    with open(fn_monthlyraw, 'w+') as writef:
        writef.writelines(outlines)
        

    outlines = []
    for yeakey, yeastat in yearly.items():
        outlines.append(lines[0]) #header
        for layername, stats in yeastat.items():
            statstr = ""
            for statkey in headers:
                statstr += (',' + str(stats[statkey]))
            outlines.append(yeakey + ',' + layername + statstr + '\n')
    
    with open(fn_yearlyraw, 'w+') as writef:
        writef.writelines(outlines)


def getStats(lines):
    layers = []
    resp = requests.get("https://map.geo.admin.ch/configs/en/layersConfig.json")
    if resp.status_code is not 200:
        print("Could not get configuration, try again later")
        return []
    jj = resp.json()
    layers = list(jj.keys())
    layers.extend(oereb_layers)
    
    now = datetime.now()
    lastline = lines[-1]
    lastdate = datetime.strptime(lastline.split(',')[0],dateformat)
    current = lastdate + timedelta(days=1)
    print(str(now) + ' | lastline: ' + lastline)
    while (current + timedelta(days=1)) < (now - timedelta(hours=3)):
        currstr = current.strftime(dateformat)
        newlines = getStatlines(current, currstr, layers)
        lines.extend(newlines)
        with open(fn_raw, 'w+') as writef:
            writef.writelines(lines)

        aggregate(lines)
        upload()
 
        current = current + timedelta(days=1)
    return lines


def upload():
    # create gzip version
    now = datetime.now()
    try:
        sub = subprocess.check_output('gzip -c ' + fn_raw + ' > ' + fn_gzip, shell = True)
        print(sub)
        sub = subprocess.check_output('gzip -c ' + fn_monthlyraw + ' > ' + fn_monthlygzip, shell = True)
        print(sub)
        sub = subprocess.check_output('gzip -c ' + fn_yearlyraw + ' > ' + fn_yearlygzip, shell = True)
        print(sub)
    except Exception as e:
        print('exception while creating zip version of file', e)

    if os.path.isfile(fn_gzip):
        copyfile(fn_gzip, 'out/layers_current_' + now.strftime(dateformat) + '.csv')

    print('Uploading...')

    try:
        sub = subprocess.check_output('aws s3 sync --cache-control "public, max-age=120" --content-encoding "gzip" --profile ltjeg_aws_admin out s3://cms.geo.admin.ch/stats/counts/', shell = True)
        print(sub)

    except Exception as e:
        print('exception while trying to upload...', e)

def main():
    lines = []
    if os.path.isfile(fn_raw):
        with open(fn_raw, 'r') as readf:
            lines = readf.readlines()
    
    if len(lines) == 0:
        lines.append(csvheader())

    while True:
        try:
            lines = getStats(lines)
            upload()
        except Exception as e:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            print message
        time.sleep(3600)

if __name__ == '__main__':
    main() 

